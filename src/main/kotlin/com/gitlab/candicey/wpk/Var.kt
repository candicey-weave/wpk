package com.gitlab.candicey.wpk

import com.gitlab.candicey.zenithcore.HOME
import net.minecraft.client.settings.KeyBinding
import java.io.File
import java.util.jar.JarFile
import java.util.logging.Logger

val LOGGER: Logger = Logger.getLogger("WPK")

val weaveDirectory by lazy { File(HOME, ".weave").also(File::mkdirs) }
val modsDirectory by lazy { File(weaveDirectory, "mods").also(File::mkdirs) }

val keyBindingMap: MutableMap<String, KeyBinding> = mutableMapOf()

val classes: Set<Class<*>> by lazy {
    val modJars = modsDirectory.listFiles()?.filter { it.name.endsWith(".jar") } ?: emptyList()
    val wpkMainEntryName = WpkMain::class.java.name.replace('.', '/') + ".class"
    val wpkJarClasses = modJars
        .map { JarFile(it) }
        .map { jarFile -> jarFile.entries().toList().filter { it.name.endsWith(".class") } }
        .find { it.any { entry -> entry.name == wpkMainEntryName } }
    wpkJarClasses?.mapNotNull { entry -> runCatching { Class.forName(entry.name.replace('/', '.').replace(".class", "")) }.getOrNull() }?.toSet() ?: emptySet()
}