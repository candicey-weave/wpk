package com.gitlab.candicey.wpk.impl

import com.gitlab.candicey.zenithcore.child.GuiScreenChild
import com.gitlab.candicey.zenithcore.helper.ScaledResolutionHelper
import io.github.kurrycat.mpkmod.compatibility.API
import io.github.kurrycat.mpkmod.compatibility.MCClasses.InputConstants
import io.github.kurrycat.mpkmod.compatibility.MCClasses.Profiler
import io.github.kurrycat.mpkmod.gui.MPKGuiScreen
import io.github.kurrycat.mpkmod.util.Vector2D
import net.minecraft.client.Minecraft
import org.lwjgl.input.Keyboard
import org.lwjgl.input.Mouse

class MPKGuiScreen(var eventReceiver: MPKGuiScreen) : GuiScreenChild() {
    private var repeatEventsEnabled = false
    override fun initGui() {
        repeatEventsEnabled = Keyboard.areRepeatEventsEnabled()

        Keyboard.enableRepeatEvents(true)

        super.initGui()

        eventReceiver.onInit()
    }

    override fun onResize(mcIn: Minecraft, width: Int, height: Int) {
        super.onResize(mcIn, width, height)

        eventReceiver.onResize(width, height)
    }

    override fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
        Profiler.startSection(if (eventReceiver.id == null) "unknown" else eventReceiver.id)

        try {
            eventReceiver.drawScreen(Vector2D(mouseX.toDouble(), mouseY.toDouble()), partialTicks)
        } catch (e: Exception) {
            API.LOGGER.warn("Error in drawScreen with id: " + eventReceiver.id, e)
        }

        Profiler.endSection()
    }

    override fun onGuiClosed() {
        Keyboard.enableRepeatEvents(repeatEventsEnabled)

        super.onGuiClosed()

        eventReceiver.onGuiClosed()
    }

    override fun keyTyped(char: Char, keyCode: Int) {
        super.keyTyped(char, keyCode)

        val c = Keyboard.getEventCharacter()

        eventReceiver.onKeyEvent(InputConstants.convert(Keyboard.getEventKey()), 0, createModifiers(), false)

        if (c.code >= 32 && c.code != 127) {
            eventReceiver.onKeyEvent(c.code, 0, createModifiers(), true)
        }
    }

    override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int) {
        super.mouseClicked(mouseX, mouseY, mouseButton)

        eventReceiver.onMouseClicked(Vector2D(mouseX.toDouble(), mouseY.toDouble()), mouseButton)
    }

    override fun mouseClickMove(mouseX: Int, mouseY: Int, clickedMouseButton: Int, timeSinceLastClick: Long) {
        super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick)

        eventReceiver.onMouseClickMove(
            Vector2D(mouseX.toDouble(), mouseY.toDouble()),
            clickedMouseButton,
            timeSinceLastClick
        )
    }

    override fun mouseReleased(mouseX: Int, mouseY: Int, state: Int) {
        super.mouseReleased(mouseX, mouseY, state)

        eventReceiver.onMouseReleased(Vector2D(mouseX.toDouble(), mouseY.toDouble()), state)
    }

    override fun handleMouseInput() {
        super.handleMouseInput()

        if (Mouse.getEventDWheel() != 0) {
            val scaledResolution = ScaledResolutionHelper.scaledResolution

            eventReceiver.onMouseScroll(
                Vector2D(
                    (Mouse.getX() * scaledResolution.scaledWidth / mc.displayWidth).toDouble(),
                    (scaledResolution.scaledHeight - Mouse.getY() * scaledResolution.scaledHeight / mc.displayHeight - 1).toDouble()
                ),
                Mouse.getEventDWheel() / 40
            )
        }
    }

    override fun doesGuiPauseGame(): Boolean = false

    companion object {
        fun createModifiers(): Int {
            var i = 0

            if (isShiftKeyDown()) i = i or 1
            if (isCtrlKeyDown())  i = i or 2
            if (isAltKeyDown())   i = i or 4

            return i
        }
    }
}
