package com.gitlab.candicey.wpk.impl

//import io.github.kurrycat.mpkmod.compatibility.MCClasses.*
import com.gitlab.candicey.zenithcore.mc
import io.github.kurrycat.mpkmod.compatibility.MCClasses.*
import io.github.kurrycat.mpkmod.ticks.TickInput
import io.github.kurrycat.mpkmod.util.BoundingBox3D
import io.github.kurrycat.mpkmod.util.Debug
import io.github.kurrycat.mpkmod.util.Vector2D
import io.github.kurrycat.mpkmod.util.Vector3D
import net.minecraft.block.Block
import net.minecraft.client.Minecraft
import net.minecraft.client.audio.PositionedSoundRecord
import net.minecraft.client.gui.ScaledResolution
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.Tessellator
import net.minecraft.client.renderer.vertex.DefaultVertexFormats
import net.minecraft.client.settings.KeyBinding
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.BlockPos
import net.minecraft.util.MathHelper
import net.minecraft.util.ResourceLocation
import net.minecraft.world.World
import net.minecraft.world.WorldType
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11
import java.awt.Color
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.util.stream.Collectors
import kotlin.math.max

class FunctionCompatibility : FunctionHolder, SoundManager.Interface, WorldInteraction.Interface, Renderer3D.Interface,
    Renderer2D.Interface, FontRenderer.Interface, io.github.kurrycat.mpkmod.compatibility.MCClasses.Minecraft.Interface,
    io.github.kurrycat.mpkmod.compatibility.MCClasses.Keyboard.Interface, Profiler.Interface {
    /**
     * Is called in [SoundManager.Interface]
     */
    override fun playButtonSound() {
        mc.soundHandler.playSound(
            PositionedSoundRecord.create(
                ResourceLocation("gui.button.press"),
                1.0f
            )
        )
    }

    /**
     * Is called in [WorldInteraction.Interface]
     */
    override fun getCollisionBoundingBoxes(blockPosVec: Vector3D): List<BoundingBox3D> {
        val blockPos = BlockPos(blockPosVec.x, blockPosVec.y, blockPosVec.z)
        val world: World = mc.theWorld
        val blockState = world.getBlockState(blockPos)
        val mask = AxisAlignedBB(
            blockPosVec.x - 1,
            blockPosVec.y - 1,
            blockPosVec.z - 1,
            blockPosVec.x + 1,
            blockPosVec.y + 1,
            blockPosVec.z + 1
        )
        val result = ArrayList<AxisAlignedBB>()
        blockState.block.addCollisionBoxesToList(world, blockPos, blockState, mask, result, null)
        return result.stream().map { aabb: AxisAlignedBB ->
            BoundingBox3D(
                Vector3D(aabb.minX, aabb.minY, aabb.minZ),
                Vector3D(aabb.maxX, aabb.maxY, aabb.maxZ)
            )
        }
            .collect(Collectors.toList())
    }

    /**
     * Is called in [WorldInteraction.Interface]
     */
    override fun getLookingAt(): Vector3D? {
        val blockPos = mc.thePlayer.rayTrace(20.0, 0f).blockPos ?: return null
        return Vector3D(blockPos.x.toDouble(), blockPos.y.toDouble(), blockPos.z.toDouble())
    }

    /**
     * Is called in [WorldInteraction.Interface]
     */
    override fun getBlockName(blockPos: Vector3D): String {
        var blockName = ""
        //if (mc.objectMouseOver != null && mc.objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK && mc.objectMouseOver.getBlockPos() != null && !(mc.thePlayer.hasReducedDebug() || mc.gameSettings.reducedDebugInfo)) {
        val blockpos = BlockPos(blockPos.x, blockPos.y, blockPos.z)
        var iblockstate = mc.theWorld.getBlockState(blockpos)
        if (mc.theWorld.worldType !== WorldType.DEBUG_WORLD) {
            iblockstate = iblockstate.block.getActualState(iblockstate, mc.theWorld, blockpos)
        }
        blockName = Block.blockRegistry.getNameForObject(iblockstate.block).toString()
        //}
        return blockName
    }

    override fun getBlockProperties(blockPos: Vector3D): HashMap<String, String> {
        val properties = HashMap<String, String>()
        val blockpos = BlockPos(blockPos.x, blockPos.y, blockPos.z)
        var iblockstate = mc.theWorld.getBlockState(blockpos)
        if (mc.theWorld.worldType !== WorldType.DEBUG_WORLD) {
            iblockstate = iblockstate.block.getActualState(iblockstate, mc.theWorld, blockpos)
        }
        for ((key, value) in iblockstate.properties) {
            properties[key.name] = value.toString()
        }
        return properties
    }

    /**
     * Is called in [Renderer3D.Interface]
     */
    override fun drawBox(bb: BoundingBox3D, color: Color, partialTicks: Float) {
        val r = color.red
        val g = color.green
        val b = color.blue
        val a = color.alpha
        GlStateManager.pushMatrix()
        GlStateManager.disableTexture2D()
        GlStateManager.disableAlpha()
        GlStateManager.enableBlend()
        GL11.glLineWidth(2.0f)
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        val tessellator = Tessellator.getInstance()
        val wr = tessellator.worldRenderer
        val entity = mc.renderViewEntity
        val entityX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * partialTicks.toDouble()
        val entityY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * partialTicks.toDouble()
        val entityZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * partialTicks.toDouble()
        wr.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR)
        wr.setTranslation(-entityX, -entityY, -entityZ)
        wr.pos(bb.minX(), bb.maxY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.maxY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.minY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.minY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.minY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.minY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.maxY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.maxY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.minY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.minY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.minY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.minY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.maxY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.maxY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.maxY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.maxY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.minY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.maxY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.maxY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.minX(), bb.minY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.minY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.maxY(), bb.minZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.maxY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.pos(bb.maxX(), bb.minY(), bb.maxZ()).color(r, g, b, a).endVertex()
        wr.setTranslation(0.0, 0.0, 0.0)
        tessellator.draw()
        GlStateManager.enableTexture2D()
        GlStateManager.enableAlpha()
        GlStateManager.disableBlend()
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.popMatrix()
    }

    /**
     * Is called in [Renderer2D.Interface]
     */
    override fun drawRect(pos: Vector2D, size: Vector2D, color: Color) {
        val r = color.red
        val g = color.green
        val b = color.blue
        val a = color.alpha
        val x = pos.x
        val y = pos.y
        val w = size.x
        val h = size.y
        val tessellator = Tessellator.getInstance()
        val wr = tessellator.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        //GlStateManager.shadeModel(GL11.GL_SMOOTH); // - for gradients
        wr.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR)
        wr.pos(x, y + h, 0.0).color(r, g, b, a).endVertex()
        wr.pos(x + w, y + h, 0.0).color(r, g, b, a).endVertex()
        wr.pos(x + w, y, 0.0).color(r, g, b, a).endVertex()
        wr.pos(x, y, 0.0).color(r, g, b, a).endVertex()
        tessellator.draw()
        //GlStateManager.shadeModel(GL11.GL_FLAT);
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    /**
     * Is called in [Renderer2D.Interface]
     */
    override fun drawLines(points: Collection<Vector2D>, color: Color) {
        if (points.size < 2) {
            Debug.stacktrace("At least two points expected, got: " + points.size)
            return
        }
        val r = color.red
        val g = color.green
        val b = color.blue
        val a = color.alpha
        GlStateManager.pushMatrix()
        GlStateManager.disableTexture2D()
        GlStateManager.disableAlpha()
        GlStateManager.enableBlend()
        GL11.glLineWidth(1.0f)
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        val tessellator = Tessellator.getInstance()
        val wr = tessellator.worldRenderer
        wr.begin(GL11.GL_LINE_STRIP, DefaultVertexFormats.POSITION_COLOR)
        for (p in points) {
            wr.pos(p.x, p.y, 0.0).color(r, g, b, a).endVertex()
        }
        wr.setTranslation(0.0, 0.0, 0.0)
        tessellator.draw()
        GlStateManager.enableTexture2D()
        GlStateManager.enableAlpha()
        GlStateManager.disableBlend()
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.popMatrix()
    }

    /**
     * Is called in [Renderer2D.Interface]
     */
    override fun getScaledSize(): Vector2D {
        val r = ScaledResolution(mc)
        return Vector2D(
            r.scaledWidth_double,
            r.scaledHeight_double
        )
    }

    override fun getScreenSize(): Vector2D {
        return Vector2D(
            mc.displayWidth.toDouble(),
            mc.displayHeight.toDouble()
        )
    }

    override fun enableScissor(x: Double, y: Double, w: Double, h: Double) {
        GL11.glEnable(GL11.GL_SCISSOR_TEST)
        val r = ScaledResolution(mc)
        val scaleFactor = r.scaleFactor.toDouble()
        val posX = x * scaleFactor
        val posY = mc.displayHeight - (y + h) * scaleFactor
        val width = w * scaleFactor
        val height = h * scaleFactor
        GL11.glScissor(
            posX.toInt(), posY.toInt(), max(0.0, width.toInt().toDouble()).toInt(), max(
                0.0, height.toInt()
                    .toDouble()
            )
                .toInt()
        )
    }

    override fun disableScissor() {
        GL11.glDisable(GL11.GL_SCISSOR_TEST)
    }

    /**
     * Is called in [FontRenderer.Interface]
     */
    override fun drawString(text: String, x: Double, y: Double, color: Color, fontSize: Double, shadow: Boolean) {
        GlStateManager.enableBlend()
        GlStateManager.pushMatrix()
        GlStateManager.translate(x, y, 0.0)
        val scale = fontSize / (mc.fontRendererObj.FONT_HEIGHT * 1f)
        GlStateManager.scale(scale, scale, 1.0)
        mc.fontRendererObj.drawString(
            text, 0f, 0f, color.rgb, shadow
        )
        GlStateManager.popMatrix()
        GlStateManager.disableBlend()
    }

    /**
     * Is called in [FontRenderer.Interface]
     */
    override fun getStringSize(text: String, fontSize: Double): Vector2D {
        val scale = fontSize / (mc.fontRendererObj.FONT_HEIGHT * 1f)
        return Vector2D(
            mc.fontRendererObj.getStringWidth(text) * scale,
            fontSize
        )
    }

    /**
     * Is called in [Minecraft.Interface][io.github.kurrycat.mpkmod.compatibility.MCClasses.Minecraft.Interface]
     */
    override fun getIP(): String {
        val d = mc.currentServerData
        return if (d == null) "Multiplayer" else d.serverIP
    }

    /**
     * Is called in [Minecraft.Interface][io.github.kurrycat.mpkmod.compatibility.MCClasses.Minecraft.Interface]
     */
    override fun getFPS(): String {
        return Minecraft.getDebugFPS().toString()
    }

    /**
     * Is called in [Minecraft.Interface][io.github.kurrycat.mpkmod.compatibility.MCClasses.Minecraft.Interface]
     */
    override fun displayGuiScreen(screen: io.github.kurrycat.mpkmod.gui.MPKGuiScreen?) {
        mc.addScheduledTask {
            mc.displayGuiScreen(
                if (screen == null) null else MPKGuiScreen(screen)
            )
        }
    }

    /**
     * Is called in [Minecraft.Interface][io.github.kurrycat.mpkmod.compatibility.MCClasses.Minecraft.Interface]
     */
    override fun getCurrentGuiScreen(): String? {
        val curr = mc.currentScreen
        if (curr == null) return null else if (curr is MPKGuiScreen) {
            var id = curr.eventReceiver.id
            if (id == null) id = "unknown"
            return id
        }
        return curr.javaClass.simpleName
    }

    /**
     * Is called in [Minecraft.Interface][io.github.kurrycat.mpkmod.compatibility.MCClasses.Minecraft.Interface]
     */
    override fun getUserName(): String? {
        return if (mc.thePlayer == null) null else mc.thePlayer.name
    }

    override fun copyToClipboard(content: String) {
        val selection = StringSelection(content)
        val clipboard = Toolkit.getDefaultToolkit().systemClipboard
        clipboard.setContents(selection, selection)
    }

    override fun setInputs(inputs: TickInput): Boolean {
        if (!io.github.kurrycat.mpkmod.compatibility.MCClasses.Minecraft.isSingleplayer()) return false
        val player = mc.thePlayer
        val gs = mc.gameSettings
        val prevPitch = player.rotationPitch
        val prevYaw = player.rotationYaw
        player.rotationYaw = (player.rotationYaw.toDouble() + inputs.yaw.toDouble()).toFloat()
        player.rotationPitch = (player.rotationPitch.toDouble() - inputs.pitch.toDouble()).toFloat()
        player.rotationPitch = MathHelper.clamp_float(player.rotationPitch, -90.0f, 90.0f)
        player.prevRotationPitch += player.rotationPitch - prevPitch
        player.prevRotationYaw += player.rotationYaw - prevYaw
        val keys = intArrayOf(
            gs.keyBindForward.keyCode,
            gs.keyBindLeft.keyCode,
            gs.keyBindBack.keyCode,
            gs.keyBindRight.keyCode,
            gs.keyBindSprint.keyCode,
            gs.keyBindSneak.keyCode,
            gs.keyBindJump.keyCode
        )
        for (i in keys.indices) {
            KeyBinding.setKeyBindState(keys[i], inputs[1 shl i])
            if (inputs[1 shl i]) KeyBinding.onTick(keys[i])
        }
        KeyBinding.setKeyBindState(gs.keyBindAttack.keyCode, inputs.l > 0)
        if (inputs.l > 0) for (i in 0 until inputs.l) KeyBinding.onTick(gs.keyBindAttack.keyCode)
        KeyBinding.setKeyBindState(gs.keyBindUseItem.keyCode, inputs.r > 0)
        if (inputs.r > 0) for (i in 0 until inputs.r) KeyBinding.onTick(gs.keyBindUseItem.keyCode)
        return true
    }

    override fun isF3Enabled(): Boolean {
        return mc.gameSettings.showDebugInfo
    }

    /**
     * Is called in [Keyboard.Interface][io.github.kurrycat.mpkmod.compatibility.MCClasses.Keyboard.Interface]
     */
    override fun getPressedButtons(): List<Int> {
        val keysDown: MutableList<Int> = ArrayList()
        for (i in 0 until Keyboard.getKeyCount()) if (Keyboard.isKeyDown(i)) keysDown.add(InputConstants.convert(i))
        return keysDown
    }

    /**
     * Is called in [Profiler.Interface]
     */
    override fun startSection(name: String) {
        mc.mcProfiler.startSection(name)
    }

    /**
     * Is called in [Profiler.Interface]
     */
    override fun endStartSection(name: String) {
        mc.mcProfiler.endStartSection(name)
    }

    /**
     * Is called in [Profiler.Interface]
     */
    override fun endSection() {
        mc.mcProfiler.endSection()
    }
}
