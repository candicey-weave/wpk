package com.gitlab.candicey.wpk.impl

import com.gitlab.candicey.wpk.keyBindingMap
import com.gitlab.candicey.zenithcore.mc
import io.github.kurrycat.mpkmod.compatibility.API
import io.github.kurrycat.mpkmod.compatibility.MCClasses.InputConstants
import io.github.kurrycat.mpkmod.compatibility.MCClasses.Player
import io.github.kurrycat.mpkmod.ticks.ButtonMS
import io.github.kurrycat.mpkmod.ticks.ButtonMSList
import io.github.kurrycat.mpkmod.util.Mouse
import io.github.kurrycat.mpkmod.util.Vector3D
import net.minecraft.client.settings.KeyBinding
import net.weavemc.loader.api.event.*
import org.lwjgl.input.Keyboard

object EventListener {
    private val timeQueue = ButtonMSList()

    @SubscribeEvent
    fun onEvent(event: KeyboardEvent) {
        val keyCode = Keyboard.getEventKey()
        val key = Keyboard.getKeyName(keyCode)
        val pressed = Keyboard.getEventKeyState()
        val gameSettings = mc.gameSettings
        val keys = intArrayOf(
            gameSettings.keyBindForward.keyCode,
            gameSettings.keyBindLeft.keyCode,
            gameSettings.keyBindBack.keyCode,
            gameSettings.keyBindRight.keyCode,
            gameSettings.keyBindSprint.keyCode,
            gameSettings.keyBindSneak.keyCode,
            gameSettings.keyBindJump.keyCode
        )

        for ((i, k) in keys.withIndex()) {
            if (keyCode == k) {
                timeQueue.add(
                    ButtonMS.of(
                        ButtonMS.Button.values()[i],
                        Keyboard.getEventNanoseconds(),
                        pressed
                    )
                )
            }
        }

        API.Events.onKeyInput(InputConstants.convert(keyCode), key, pressed)

        keyBindingMap.forEach { (id: String?, keyBinding: KeyBinding) ->
            val keyBindingPressed = keyBinding.isPressed

            if (keyBindingPressed && API.guiScreenMap.containsKey(id)) {
                val eventReceiver = API.guiScreenMap[id] ?: run {
                    API.LOGGER.warn("GuiScreen with id: $id not found")
                    return@forEach
                }

                mc.displayGuiScreen(MPKGuiScreen(eventReceiver))
            }

            if (keyBindingPressed && API.keyBindingMap.containsKey(id)) {
                API.keyBindingMap[id]!!.run()
            }
        }
    }

    @SubscribeEvent
    fun onMouseEvent(event: MouseEvent) {
        API.Events.onMouseInput(
            Mouse.Button.fromInt(event.button),
            if (event.button == -1) Mouse.State.NONE else if (event.buttonState) Mouse.State.DOWN else Mouse.State.UP,
            event.x,
            event.y,
            event.dx,
            event.dy,
            event.dwheel,
            event.nanoseconds
        )
    }


    @SubscribeEvent
    fun onTick(event: TickEvent) {
        val mcPlayer = mc.thePlayer
        if (mcPlayer != null && event is TickEvent.Pre) {
            Player()
                .setPos(Vector3D(mcPlayer.posX, mcPlayer.posY, mcPlayer.posZ))
                .setLastPos(Vector3D(mcPlayer.lastTickPosX, mcPlayer.lastTickPosY, mcPlayer.lastTickPosZ))
                .setMotion(Vector3D(mcPlayer.motionX, mcPlayer.motionY, mcPlayer.motionZ))
                .setRotation(mcPlayer.rotationYaw, mcPlayer.rotationPitch)
                .setOnGround(mcPlayer.onGround)
                .setSprinting(mcPlayer.isSprinting)
                .constructKeyInput()
                .setKeyMSList(timeQueue.copy())
                .buildAndSave()

            timeQueue.clear()
        }

        if (event is TickEvent.Pre) {
            API.Events.onTickStart()
        } else if (event is TickEvent.Post) {
            API.Events.onTickEnd()
        }
    }

    @SubscribeEvent
    fun onRender(event: RenderGameOverlayEvent) {
        API.Events.onRenderOverlay()
    }

    @SubscribeEvent
    fun onRenderWorld(event: RenderWorldEvent) {
        API.Events.onRenderWorldOverlay(event.partialTicks)
    }

    /*@SubscribeEvent
    fun onServerConnect(e: FMLNetworkEvent.ClientConnectedToServerEvent) {
        API.Events.onServerConnect(e.isLocal)
    }

    @SubscribeEvent
    fun onServerDisconnect(e: FMLNetworkEvent.ClientDisconnectionFromServerEvent?) {
        API.Events.onServerDisconnect()
    }*/
}