package com.gitlab.candicey.wpk

import com.gitlab.candicey.wpk.impl.EventListener
import com.gitlab.candicey.wpk.impl.FunctionCompatibility
import com.gitlab.candicey.zenithcore.hook.LocaleHook
import com.gitlab.candicey.zenithcore.keybind.KeyBind
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.Dependencies.zenithCore
import com.gitlab.candicey.zenithloader.dependency.version
import io.github.kurrycat.mpkmod.compatibility.API
import io.github.kurrycat.mpkmod.gui.MPKGuiScreen
import io.github.kurrycat.mpkmod.util.Procedure
import net.minecraft.client.Minecraft
import net.minecraft.client.settings.GameSettings
import net.weavemc.loader.api.ModInitializer
import net.weavemc.loader.api.event.EventBus
import net.weavemc.loader.api.event.StartGameEvent
import org.lwjgl.input.Keyboard
import java.io.File

class WpkMain : ModInitializer {
    override fun preInit() {
        info("Initialising...")

        ZenithLoader.loadDependencies(
            zenithCore version "@@ZENITH_CORE_VERSION@@",
        )

        EventBus.subscribe(StartGameEvent.Post::class.java) {
            API.preInit(javaClass)

            val idList = mutableListOf<String>()

            for ((id: String, guiScreen: MPKGuiScreen) in API.guiScreenMap) {
                if (guiScreen.shouldCreateKeyBind()) {
                    idList.add(id)
                }
            }

            for ((id: String, _: Procedure?) in API.keyBindingMap) {
                idList.add(id)
            }

            registerKeyBindings(idList)

            API.LOGGER.info(API.COMPATIBILITY_MARKER, "Registering compatibility functions...")
            API.registerFunctionHolder(FunctionCompatibility())
            API.LOGGER.info(API.COMPATIBILITY_MARKER, "Done")

            EventBus.subscribe(EventListener)

            registerKeyBindings()

            API.init(Minecraft.getSessionInfo()["X-Minecraft-Version"])

            API.Events.onLoadComplete()
        }

        info("Initialised!")
    }

    private fun registerKeyBindings(idList: List<String>) {
        val optionsFile = File(mc.mcDataDir, "options.txt")
        val keyOptions = runCatching { optionsFile.readLines().filter { it.startsWith("key_") } }.getOrNull() ?: emptyList()
        val map = mutableMapOf<String, Int>()
        for (line in keyOptions) {
            runCatching {
                val split = line.split(":")
                val key = split[0].substring(4) // remove "key_"
                val value = split[1].toInt()
                map[key] = value
            }
        }

        idList
            .associateWith {
                val id = "${API.MODID}.key.$it.desc"
                val key = map[id] ?: Keyboard.KEY_NONE
                KeyBind(id, key, API.KEYBINDING_CATEGORY)
            }
            .also { keyBindingMap.putAll(it) }
            .values
            .let {
                val gameSettings = mc.gameSettings
                val keyBindings = gameSettings.keyBindings
                gameSettings.keyBindings = keyBindings + it
            }
    }

    private fun registerKeyBindings() {
        LocaleHook.messages.putAll(
            listOf(
                "mpkmod.key.main_gui.desc" to "Open MPK Mod GUI",
                "mpkmod.key.lb_gui.desc" to "Open Landing Block GUI",
                "mpkmod.key.lb_set.desc" to "Set Landing Block",
                "mpkmod.key.options_gui.desc" to "Open Options GUI",
                "mpkmod.main_gui.title" to "MPK Mod GUI",
                "mpkmod.lb_gui.title" to "Landing Block GUI",
            )
        )

        for (k in mc.gameSettings.keyBindings) {
            io.github.kurrycat.mpkmod.compatibility.MCClasses.KeyBinding(
                { GameSettings.getKeyDisplayString(k.keyCode) },
                k.keyDescription,
                { k.isKeyDown }
            )
        }

        API.LOGGER.info(
            API.COMPATIBILITY_MARKER, "Registered {} Keybindings",
            io.github.kurrycat.mpkmod.compatibility.MCClasses.KeyBinding.getKeyMap().size
        )
    }
}