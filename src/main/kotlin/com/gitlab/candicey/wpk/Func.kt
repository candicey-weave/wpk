package com.gitlab.candicey.wpk

@Suppress("NOTHING_TO_INLINE")
inline fun info(message: String) = LOGGER.info("[WPK] $message")