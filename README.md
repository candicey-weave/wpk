# WPK
- A [Weave](https://github.com/Weave-MC) mod implementation of [MPKMod 2](https://github.com/kurrycat2004/MPKMod_2/) mod.

<br>

## Images
![Ingame](assets/ingame.png)

<br>

## Installation
1. Download the [WPK](#download) mod.
2. Place the jars in your Weave mods folder.
    1. Windows: `%userprofile%\.weave\mods`
    2. Unix: `~/.weave/mods`

<br>

## Build
1. Clone the repository.
2. Run `./gradlew build` in the root directory.
3. The built jar file will be in `build/libs`.

<br>

## Credits
- [MPKMod 2](https://github.com/kurrycat2004/MPKMod_2) - For the original mod and code.
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.

<br>

## Download
- [Package Registry](https://gitlab.com/candicey-weave/wpk/-/packages) - Select the latest version and download the jar file that ended with `-relocated.jar`.

<br>

## License
- WPK is licensed under the [GNU General Public License Version 3](LICENSE).